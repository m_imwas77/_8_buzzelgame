InitState = [];
state = [[1,2,3],[4,5,6],[7,8,0]];
pairs = []
indexs = []
ids = [[1,2,3],[4,5,6],[7,8,0]];
solves = [[1,2,3],[4,5,6],[7,8,0]];
function play(id){
	if(isWon()){
		if(confirm("you won do you want to contunio play")){
			startGame();
		}else{
			return ;
		}
	}else{
		var fir = pairs[id].first;
		var sec = pairs[id].second;
		if(fir + 1 < 3){
			if(state[fir+1][sec] === 0){
				var tempState = state[fir+1][sec];
				var tempId = ids[fir+1][sec];
				document.getElementById(id).innerHTML = tempState;
				document.getElementById(tempId).innerHTML = state[fir][sec];
				state[fir+1][sec] = state[fir][sec];
				state[fir][sec] = tempState;
			}
		}
		if(fir - 1 >= 0){
			if(state[fir-1][sec] === 0){
				var tempState = state[fir-1][sec];
				var tempId = ids[fir-1][sec];
				document.getElementById(id).innerHTML = tempState;
				document.getElementById(tempId).innerHTML = state[fir][sec];
				state[fir-1][sec] = state[fir][sec];
				state[fir][sec] = tempState;
			}
		}
		if(sec + 1 < 3){
			if(state[fir][sec+1] === 0){
				var tempState = state[fir][sec+1];
				var tempId = ids[fir][sec+1];
				document.getElementById(id).innerHTML = tempState;
				document.getElementById(tempId).innerHTML = state[fir][sec];
				state[fir][sec+1] = state[fir][sec];
				state[fir][sec] = tempState;
			}
		} 
		if(sec - 1 >= 0){
			if(state[fir][sec-1] === 0){
				var tempState = state[fir][sec-1];
				var tempId = ids[fir][sec-1];
				document.getElementById(id).innerHTML = tempState;
				document.getElementById(tempId).innerHTML = state[fir][sec];
				state[fir][sec-1] = state[fir][sec];
				state[fir][sec] = tempState;
			}
		}
	}
	
	for(var i = 0 ; i< 3;i++){
		for(var j=0;j<3;j++){
			if(state[i][j] == solves[i][j]){
				document.getElementById(ids[i][j]).style = "background:red";
			}
			else if(solves[i][j] == 0){
				document.getElementById(ids[i][j]).style= "background :black";
			}
			else{
				document.getElementById(ids[i][j]).style = "background-color: brown;";
			}
		}
	}
}

function startGame(){
	id = 0;

	for(var i=0;i<3;i++){
		for(var j = 0 ; j<3;j++){
			var pair = {
				first :  i ,
				second : j
			}
			pairs.push(pair);
			ids[i][j] = id++;
			solves[i][j] = id;
		}
	}
	var number = [1,2,3,4,5,6,7,8,0];
	InitState = [];
	for(var i=0;i<9;i++){
		var element = document.getElementById(i);
		var index = Math.floor(Math.random()*number.length);
		element.innerHTML = number[index]; 
		InitState.push(number[index]);
		number.splice(index, 1);
	}
	var loop = 0;
	for(var i = 0 ;i<3;i++){
		for(var j=0;j<3;j++){
			state[i][j] = InitState[loop++];
		}
	}
	while(isWon()){
		startGame();
	}
}

function isWon(){
	return (state[0][0] == 1 
		   && state[0][1] == 2
		   && state[0][2] == 3
		   && state[1][0] == 4
		   && state[1][1] == 5
		   && state[1][2] == 6
		   && state[2][0] == 7
		   && state[2][1] == 8
		   && state[2][2] == 0);
}